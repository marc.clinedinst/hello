import requests

if __name__ == '__main__':
    response = requests.get("https://jsonplaceholder.typicode.com/posts")
    response_content = response.json()

    print(response_content)
